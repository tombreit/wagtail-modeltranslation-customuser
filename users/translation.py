from modeltranslation.translator import TranslationOptions
from modeltranslation.decorators import register

from .models import CustomUser


@register(CustomUser)
class CustomUserTR(TranslationOptions):
    pass
