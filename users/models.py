from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    country = models.CharField(verbose_name='country', max_length=255)
