# Wagtail demo project

Combine custom user and wagtail-modeltranslation


## This project

1. Installed wagtail
1. Created blank demo project: ``wagtail start demo``
1. Added custom user: https://docs.wagtail.io/en/v2.4/advanced_topics/customisation/custom_user_models.html
1. Added wagtail-modeltranslation: https://github.com/infoportugal/wagtail-modeltranslation
